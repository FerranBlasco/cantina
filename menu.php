
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php"); ?>
	<title>Menú</title>
</head>
<body onload= "hora()">
	
	<div class="progress">
		<div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 33%;" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100"></div>
	</div>

	<hr/>	
	<?php ?>
    <!--Menu Dinar-->
    <div class="div_menu" id="dinar">
		<div class="titol_menu">Menú dinar</div>
			<div style="display: flex; flex-direction: row;">
				<div class="menu_columna1 nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="nav-link active" id="v-pills-cafes_dinar-tab" data-toggle="pill" href="#v-pills-cafes_dinar" role="tab" aria-controls="v-pills-cafes_dinar" aria-selected="true">Cafés</a>
					<a class="nav-link" id="v-pills-plats-tab" data-toggle="pill" href="#v-pills-plats" role="tab" aria-controls="v-pills-plats" aria-selected="false">Plats</a>
					<a class="nav-link" id="v-pills-begudes_dinar-tab" data-toggle="pill" href="#v-pills-begudes_dinar" role="tab" aria-controls="v-pills-begudes_dinar" aria-selected="false">Begudes</a>
					<a class="nav-link" id="v-pills-complements_dinar-tab" data-toggle="pill" href="#v-pills-complements_dinar" role="tab" aria-controls="v-pills-complements_dinar" aria-selected="false">Complements</a>
				</div>
				<div class="menu_columna2 tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-cafes_dinar" role="tabpanel" aria-labelledby="v-pills-cafes_dinar-tab">
						<div class="flex-container_item">
								<div class="container_nom bold" > Producte </div> 
								<div class="container_preu bold"> Preu </div>
								<div class="container_select bold"> Quantitat </div>
							</div>
						<div class="flex-container_menu">
								<?php
								require_once("items.php");
								echo('<form id="form" action="comanda.php" method="post">');
								foreach ($items as $key => $item){ 
									if( $key>=0 && $key<=4 ){
										echo('
										<div class="flex-container_item">
											<div class="container_nom"> '.$item[0].' </div> 
											<div class="container_preu"> '.$item[1].' € </div> 
											<div class_"container_select">
												<input type="number" name="'.$key.'" id="'.$key.'d'.'" value="0"> 
												<input type="button" onclick="resta1(\''.$key.'d'.'\')" value="-">
												<input type="button" onclick="suma1(\''.$key.'d'.'\')" value="+">
											</div>
										</div>
										'
										);
									}
								}
								?>
							</div>

					</div>
					<div class="tab-pane fade" id="v-pills-plats" role="tabpanel" aria-labelledby="v-pills-plats-tab">
						<div class="flex-container_item">
							<div class="container_nom bold" > Plat </div> 
							<div class="container_preu bold"> Preu </div>
							<div class="container_select bold"> Quantitat </div>
						</div>
						<div class="flex-container_menu">
							<?php
							require_once("items.php");
							foreach ($items as $key => $item){
								if( $key>=27 && $key<=32 ){
									echo('
									<div class="flex-container_item">
										<div class="container_nom"> '.$item[0].' </div> 
										<div class="container_preu"> '.$item[1].' € </div> 
										<div class_"container_select">
											<input type="number" name="'.$key.'" id="'.$key.'d'.'" value="0"> 
											<input type="button" onclick="resta1(\''.$key.'d'.'\')" value="-">
											<input type="button" onclick="suma1(\''.$key.'d'.'\')" value="+">
										</div>
									</div>
									'
									);
								}

							}
							?>
						</div>

					</div>
					<div class="tab-pane fade" id="v-pills-begudes_dinar" role="tabpanel" aria-labelledby="v-pills-begudes_dinar-tab">
						<div class="flex-container_item">
								<div class="container_nom bold" > Producte </div> 
								<div class="container_preu bold"> Preu </div>
								<div class="container_select bold"> Quantitat </div>
						</div>
						<div class="flex-container_menu">
							<?php
								require_once("items.php");
								foreach ($items as $key => $item){
									if( $key>=16 && $key<=22 ){
										echo('
										<div class="flex-container_item">
											<div class="container_nom"> '.$item[0].' </div> 
											<div class="container_preu"> '.$item[1].' € </div> 
											<div class_"container_select">
												<input type="number" name="'.$key.'" id="'.$key.'d'.'" value="0"> 
												<input type="button" onclick="resta1(\''.$key.'d'.'\')" value="-">
												<input type="button" onclick="suma1(\''.$key.'d'.'\')" value="+">
											</div>
										</div>
										'
										);
									}

								}
							?>
						</div>
					</div>
					<div class="tab-pane fade" id="v-pills-complements_dinar" role="tabpanel" aria-labelledby="v-pills-complements_dinar-tab">
						<div class="flex-container_item">
							<div class="container_nom bold" > Producte </div> 
							<div class="container_preu bold"> Preu </div>
							<div class="container_select bold"> Quantitat </div>
						</div>
						<div class="flex-container_menu">
							<?php
								require_once("items.php");
									foreach ($items as $key => $item){ 
										if( $key>=23 && $key<=25 ){
											echo('
											<div class="flex-container_item">
												<div class="container_nom"> '.$item[0].' </div> 
												<div class="container_preu"> '.$item[1].' € </div> 
												<div class_"container_select">
													<input type="number" name="'.$key.'" id="'.$key.'d'.'" value="0"> 
													<input type="button" onclick="resta1(\''.$key.'d'.'\')" value="-">
													<input type="button" onclick="suma1(\''.$key.'d'.'\')" value="+">
												</div>
											</div>
											'
											);
										}

								}
							?>
						</div>
					</div>
					<?php
						echo('<div class="menu_columna1"></div>
						<div class="menu_columna2"><input class="btn btn-primary" type="submit" value="confirma la teva comanda"></div></form>'); 
					?>
				</div>
			</div>
	</div>
	
	<!--Menu Pati-->
	<div class="div_menu" id="pati">
		<div class="titol_menu">Menú pati</div>
			<div style="display: flex; flex-direction: row;">
				<div class="menu_columna1 nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="nav-link active" id="v-pills-cafes_pati-tab" data-toggle="pill" href="#v-pills-cafes_pati" role="tab" aria-controls="v-pills-cafes_pati" aria-selected="true">Cafés</a>
					<a class="nav-link" id="v-pills-entrepans-tab" data-toggle="pill" href="#v-pills-entrepans" role="tab" aria-controls="v-pills-entrepans" aria-selected="false">Entrepans</a>
					<a class="nav-link" id="v-pills-begudes_pati-tab" data-toggle="pill" href="#v-pills-begudes_pati" role="tab" aria-controls="v-pills-begudes_pati" aria-selected="false">Begudes</a>
					<a class="nav-link" id="v-pills-complements_pati-tab" data-toggle="pill" href="#v-pills-complements_pati" role="tab" aria-controls="v-pills-complements_pati" aria-selected="false">Complements</a>
				</div>
				<div class="menu_columna2 tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-cafes_pati" role="tabpanel" aria-labelledby="v-pills-cafes_pati-tab">
						<div class="flex-container_item">
								<div class="container_nom bold" > Producte </div> 
								<div class="container_preu bold"> Preu </div>
								<div class="container_select bold"> Quantitat </div>
							</div>
						<div class="flex-container_menu">
								<?php
								require_once("items.php");
								echo('<form id="form" action="comanda.php" method="post">');
								foreach ($items as $key => $item){ 
									if( $key>=0 && $key<=4 ){
										echo('
										<div class="flex-container_item">
											<div class="container_nom"> '.$item[0].' </div> 
											<div class="container_preu"> '.$item[1].' € </div> 
											<div class_"container_select">
												<input type="number" name="'.$key.'" id="'.$key.'p'.'" value="0"> 
												<input type="button" onclick="resta1(\''.$key.'p'.'\')" value="-">
												<input type="button" onclick="suma1(\''.$key.'p'.'\')" value="+">
											</div>
										</div>
										'
										);
									}
								}
								?>
							</div>
					</div>
					<div class="tab-pane fade" id="v-pills-entrepans" role="tabpanel" aria-labelledby="v-pills-entrepans-tab">
						<div class="flex-container_item">
							<div class="container_nom bold" > Producte </div> 
							<div class="container_preu bold"> Preu </div>
							<div class="container_select bold"> Quantitat </div>
						</div>
						<div class="flex-container_menu">
							<?php
							require_once("items.php");
							foreach ($items as $key => $item){
								if( $key>=5 && $key<=15 ){
									echo('
									<div class="flex-container_item">
										<div class="container_nom"> '.$item[0].' </div> 
										<div class="container_preu"> '.$item[1].' € </div> 
										<div class_"container_select">
											<input type="number" name="'.$key.'" id="'.$key.'p'.'" value="0"> 
											<input type="button" onclick="resta1(\''.$key.'p'.'\')" value="-">
											<input type="button" onclick="suma1(\''.$key.'p'.'\')" value="+">
										</div>
									</div>
									'
									);
								} 
								
							}
							?>
						</div>
					</div>
					<div class="tab-pane fade" id="v-pills-begudes_pati" role="tabpanel" aria-labelledby="v-pills-begudes_pati-tab">
						<div class="flex-container_item">
								<div class="container_nom bold" > Producte </div> 
								<div class="container_preu bold"> Preu </div>
								<div class="container_select bold"> Quantitat </div>
						</div>
						<div class="flex-container_menu">
							<?php
								require_once("items.php");
								foreach ($items as $key => $item){ 
									if( $key>=16 && $key<=22 ){
										echo('
										<div class="flex-container_item">
											<div class="container_nom"> '.$item[0].' </div> 
											<div class="container_preu"> '.$item[1].' € </div> 
											<div class_"container_select">
												<input type="number" name="'.$key.'" id="'.$key.'p'.'" value="0"> 
												<input type="button" onclick="resta1(\''.$key.'p'.'\')" value="-">
												<input type="button" onclick="suma1(\''.$key.'p'.'\')" value="+">
											</div>
										</div>
										'
										);
									}

								}
							?>
						</div>
					</div>
					<div class="tab-pane fade" id="v-pills-complements_pati" role="tabpanel" aria-labelledby="v-pills-complements_pati-tab">
						<div class="flex-container_item">
							<div class="container_nom bold" > Producte </div> 
							<div class="container_preu bold"> Preu </div>
							<div class="container_select bold"> Quantitat </div>
						</div>
						<div class="flex-container_menu">
							<?php
								require_once("items.php");
									foreach ($items as $key => $item){ 
										if( $key>=23 && $key<=26 ){
											echo('
											<div class="flex-container_item">
												<div class="container_nom"> '.$item[0].' </div> 
												<div class="container_preu"> '.$item[1].' € </div> 
												<div class_"container_select">
													<input type="number" name="'.$key.'" id="'.$key.'p'.'" value="0"> 
													<input type="button" onclick="resta1(\''.$key.'p'.'\')" value="-">
													<input type="button" onclick="suma1(\''.$key.'p'.'\')" value="+">
												</div>
											</div>
											'
											);
										}

								}
							?>
						</div>
					</div>
					<?php 
						echo('<div class="menu_columna1"></div>
						<div class="menu_columna2"><input class="btn btn-primary" type="submit" value="Confirma la teva comanda"></div></form>'); 
					?>
				</div>
			</div>			
    </div>


    </body>
</html>
