
    <!-- Footer -->
	<footer class="page-footer font-small blue-grey lighten-5">

  

  <!-- Footer Links -->
  <div class="container text-center text-md-left mt-5">

    <!-- Grid row -->
    <div class="row mt-3 dark-grey-text">

      <!-- Grid column -->
      <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
		<div class="mapouter">
			<div class="gmap_canvas">
				<iframe width="368" height="361" id="gmap_canvas" src="https://maps.google.com/maps?q=Barcelona%2C%20Av.%20d'Esplugues%2C%2036%2C%2042&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
				<a href="https://www.embedgooglemap.net/blog/elementor-pro-discount-code-review/">elementor review</a>
			</div>
			<style>.mapouter{position:relative;text-align:right;height:361px;width:368px;}.gmap_canvas {overflow:hidden;background:none!important;height:361px;width:368px;}</style>
		</div>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

        <!-- Links -->
        <h6 class="text-uppercase font-weight-bold">Contacte</h6>
        <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>
          <i class="fas fa-home mr-3"></i> Barcelona, Av. d'Esplugues, 36, 42</p>
        <p>
          <i class="fas fa-envelope mr-3"></i> a8076391@xtec.cat</p>
        <p>
          <i class="fas fa-phone mr-3"></i> 932 033 332</p>
        <p>
          <i class="fas fa-print mr-3"></i> 932 046 212</p>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center text-black-50 py-3">© 2019:
    <a class="dark-grey-text"> Sonia Vargas, Ferran Blasco, Ismail Ait</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
