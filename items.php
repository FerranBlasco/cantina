<?php

//PATI
//Cafes 01-04
//Entrepans 05-15
//Begudes 16-22
//Complements 23-26
//Plats 27-32

$items = array (
    '01' => array ("Cafè sol", 1, "img.png"),
    '02' => array ("Cafè tallat", 1, " "),
    '03' => array ("Cafè amb llet", 1, " "),
    '04' => array ("Infusions", 1.20, " "),
    '05' => array ("Entrepà de fuet", 1.70, " "),
    '06' => array ("Entrepà de formatge", 1.70, " "),
    '07' => array ("Entrepà de pernil dolç", 2, " "),
    '08' => array ("Entrepà de pernil salat", 2, " "),
    '09' => array ("Entrepà de bacó", 2, " "),
    '10' => array ("Entrepà de llom", 2.40, " "),
    '11' => array ("Entrepà de xoriço", 2, " "),
    '12' => array ("Entrepà de frankfurt", 2, " "),
    '13' => array ("Entrepà de truita", 2, " "),
    '14' => array ("Entrepà de vegetal tonyina", 2.50, " "),
    '15' => array ("Entrepà de vegetal pollastre", 2.20, " "),
    '16' => array ("Llauna Coca-Cola", 1.20, " "),
    '17' => array ("Llauna Coca-Cola Zero", 1.20, " "),
    '18' => array ("Llauna Fanta", 1.20, " "),
    '19' => array ("Llauna Sprite", 1.20, " "),
    '20' => array ("Cacaolat", 1.20, " "),
    '21' => array ("Suc tropical", 1.20, " "),
    '22' => array ("Vichy", 1.40, " "),
    '23' => array ("Donut", 1.30, " "),
    '24' => array ("Croissant", 1, " "),
    '25' => array ("Caracola", 1.20, " "),
    '26' => array ("Pizza", 1.20, " "),
    '27' => array ("Macarrons, llom a la planxa i peça de fruita", 8.75, ""),
    '28' => array ("Amanida d'arròs, gambes a la planxa i natilla", 9.40, " "),
    '29' => array ("Sopa de peix i pastís de formatge", 6.45, " "),
    '30' => array ("Espaguetis a la bolonyesa, truita de patates i flam", 7.85, " "),
    '31' => array ("Amanida Cèsar i peça de fruita ", 5.50, " "),
    '32' => array ("Verdura, crestes de tonyina i iogurt", 8, " ")
    );


?>

