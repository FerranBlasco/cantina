//Scripts per menu.php
//Sumar i restar quantitat de productes escollits
function suma1(target) {
    document.getElementById(target).value++;

}

function resta1(target) {
    let valor = document.getElementById(target).value;
    if (valor > 0) document.getElementById(target).value--;
}

function hora(target) { //Controlar hora del dia per mostrar/amagar menús
    let datetimeNow = new Date();
    let hora_actual = datetimeNow.getHours();
    let minuts_actuals = datetimeNow.getMinutes();
    console.log("Hora actual: " + hora_actual + ":" + minuts_actuals);
    if (hora_actual == 11) {
        if (minuts_actuals > 30) {
            document.getElementById("pati").style.display = "none"; //amaga el pati
        } else { //amaga el dinar
            document.getElementById("dinar").style.display = "none";
        }
    } else if (hora_actual < 11) { //amaga el dinar
        document.getElementById("dinar").style.display = "none";
    } else { //amaga el pati
        document.getElementById("pati").style.display = "none";
    }
}


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validate() {

    //Selecció div amb alerta
    var error = document.getElementById("avis_error");
    var contingut_error = document.getElementById("info_error");

    //Validació Nom i Cognoms
    var nom = document.getElementById("inputNom").value;
    var cognoms = document.getElementById("inputCognoms").value;

    //Validació Telefon
    var numTel = document.getElementById("inputTel").value;
    if (numTel.length < 9 || numTel.length > 9) {
        error.style.display = "block";
        error.className = "alert alert-danger";
        contingut_error.innerText = "El nombre de telèfon és incorrecte!";
        return false;
    } else {
        error.style.display = "none";
        error.className = "alert alert-primary";
        contingut_error.innerText = "Omple les dades del formulari per completar la teva comanda";
    }

    var email = document.getElementById("inputEmail").value;
    console.log(email);
    if (validateEmail(email)) {
        error.style.display = "none";
        error.className = "alert alert-primary";
        contingut_error.innerText = "Omple les dades del formulari per completar la teva comanda";
        console.log(email);
    } else {
        error.style.display = "block";
        error.className = "alert alert-danger";
        contingut_error.innerText = "El email introduit és incorrecte!";
        console.log(email);
        return false;
    }

}
console.log(validate);
//$("#confirmar_dades").on("click", validate);