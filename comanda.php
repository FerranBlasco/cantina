<!DOCTYPE html>
<html>
    <head>
        <?php include("head.php"); ?>
        <title>Comanda</title>
    </head>
    <body>
        <div class="progress">
            <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 66%;" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <div id="comanda_header">
            <h4>Comanda</h4>
        </div>

        <div class="card bg-light mb-3" style="margin-left: 20%; margin-right: 20%; padding: 10px; ">
            <div class="card-header">Productes</div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-2 quantitat">Quantitat</dt>
                    <dt class="col-sm-4">Productes</dt>
                    <dt class="col-sm-3">Preu Unitari</dt>
                    <dt class="col-sm-3">Preu total</dt>
                    <?php 
                        require_once("items.php");
                        $total = 0;
                        foreach ($items as $key => $item){ 
                            $num_items = $_POST[ $key ];
                            if ( $num_items>0 ){
                                $total+= $item[1] * $num_items;
                                echo('
                                    <dt class="col-sm-2 quantitat">'.$num_items.'x</dt>
                                    <dd class="col-sm-4">'.$item[0].'</dd>
                                    <dd class="col-sm-3">'.$item[1].'€</dd> 
                                    <dd class="col-sm-3">'.$item[1] * $num_items.'€</dd> 
                                ');
                                $nom=$item[0];
                                setcookie($key,$num_items,time()+1800);
                            } 
                            setcookie("totalComanda",$total,time()+1800);
                        }
                    ?>
                </dl>
                <div>
                    <dl class="row">
                        <dd class="col-sm-6"></dd>
                        <dt class="col-sm-3">Total:</dt>
                        <dd class="col-sm-3"><?php echo($total); ?>€</dd>
                    </dl>
                </div>
            </div>
        </div>
        <br> 
        <!--Formulari dades -->
        <div id="dades">
            <h3 id="dades_titol">Dades</h3>
            <div id="avis_error" class="alert alert-primary" role="alert">
                <p id="info_error">Omple les dades del formulari per completar la teva comanda</p>
            </div>
            <form id="form_dades" method="POST" onsubmit="return validate()" action="confirm.php"> <!-- action="confirm.php" -->
                <div class="form-row">
                    <div class="col">
                        <label>Nom</label>
                        <input type="text" class="form-control" id="inputNom" placeholder="Nom" name="nomUsuari">
                    </div>
                    <div class="col">
                        <label>Cognoms</label>
                        <input type="text" class="form-control" id="inputCognoms" placeholder="Cognoms" name="cognomUsuari">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputTel">Telèfon</label>
                    <input type="text" class="form-control" id="inputTel" placeholder="Telèfon" name="telUsuari">
                </div>
                <div class="form-group">
                    <label for="inputEmail">Email</label>
                    <input type="text" class="form-control" id="inputEmail" placeholder="Email" name="mailUsuari">
                </div>
                <button id="confirmar_dades" type="submit" class="btn btn-primary">Confirmar</button>
            </form>
            
        </div>
    </body>
</html>
