<!DOCTYPE html>
<html>
<head>
    <?php include("head.php"); ?>
    <link rel= "stylesheet" type="text/css" href="css/styles.css">
    <title>ERROR</title>
</head>
<body>
    <div class="div_informar">
        <div class="card">
            <span id="error">
                <i class="fas fa-exclamation-circle"></i>
            </span>
            <div class="card-body">
                <h5 class="card-title">Ja has fet una comanda avui</h5>
                <a href="index.php" class="btn btn-error">Tornar a inici</a>
            </div>
        </div>
    </div>

</body>
</html>