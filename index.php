<!DOCTYPE html>
<html>
    <head>
		<?php 
			include("head.php"); 
			include ("capcelera.php")
        ?>
        <script src="js/scripts.js"> </script>
        <link rel= "stylesheet" type="text/css" href="css/styles.css">
    </head>
    <body id="cos_index" >
        <h1 id= "titol_index"> Cantina Pedralbes </h1>
        <div id="panel">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			  <ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			  </ol>
			  <div class="carousel-inner">
				<div class="carousel-item active">
				  <img class="d-block w-100" src="img/cantina_1.jpg" alt="First slide">
				</div>
				<div class="carousel-item">
				  <img class="d-block w-100" src="img/cantina_2.jpg" alt="Second slide">
				</div>
				<div class="carousel-item">
				  <img class="d-block w-100" src="img/cantina_3.jpg" alt="Third slide">
				</div>
			  </div>
			  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			  </a>
			</div>
		</div>
		<div id="buttons_index">
			<div class="button_index">
				<form action=
				<?php 
					if(isset($_COOKIE["comandaDiaria"]))
						echo('"error.php"');
					else 
						echo('"menu.php"');
				?>  >
					<input type="submit" class="btn btn-primary" value="Realitza la teva comanda" />
				</form> 
			</div>
			<div class="button_index">
				<form action="admin/" >
            		<input type="submit" class="btn btn-primary" value="Pagina per a administradors"/>
        		</form>   
			</div>
		</div>
        
		<hr class="my-4">
    </body>
    <?php include("footer.php"); ?>
</html>
