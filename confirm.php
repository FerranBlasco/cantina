<!DOCTYPE html>
<?php
    $minutActual = date("i");
    $horesRestants = 24 - date("H");
    $tempsRestant = ($horesRestants * 60 - $minutActual)*60; 
    setcookie("comandaDiaria",1,time()+$tempsRestant); //l'ultim camp es la duracio, volem que caduqui a les 24h
?>

<html>
<head>
    <?php include("head.php"); ?>
</head>
<body>
    <div class="progress div_final">
		<div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
	</div>
    <?php
        $nom_fitxer = "comanda_" . date("d-m-Y") . ".txt";
        $data_format_avui = date("d-m-Y H:i:s");
        $directori_fitxer = dirname(__FILE__).'/admin/comandes/'.$nom_fitxer;
        echo $directori_fitxer;
        require_once("items.php");
        file_put_contents($directori_fitxer,"//-----------------------------// \n Hora: ".$data_format_avui."".PHP_EOL."ITEMS:".PHP_EOL,FILE_APPEND);

        foreach ($items as $key => $item){
            if ( isset($_COOKIE[$key]) ){
                    file_put_contents($directori_fitxer, $item[0]." x ".$_COOKIE[$key].PHP_EOL,FILE_APPEND);
            }
        }

        file_put_contents($directori_fitxer,PHP_EOL."TOTAL: ".$_COOKIE['totalComanda'].PHP_EOL,FILE_APPEND);
        file_put_contents($directori_fitxer,"Nom i Cognoms: ".$_POST['nomUsuari']." ".$_POST['cognomUsuari'].PHP_EOL,FILE_APPEND);
        file_put_contents($directori_fitxer,"Email: ".$_POST['mailUsuari'].PHP_EOL,FILE_APPEND);
        file_put_contents($directori_fitxer,"Telefon: ".$_POST['telUsuari'].PHP_EOL,FILE_APPEND);
        file_put_contents($directori_fitxer,"///-----------------------------///".PHP_EOL,FILE_APPEND);

    ?>
    <div class="div_informar">
        <div class="card">
            <span id="confirm">
                <i class="far fa-check-circle"></i>
            </span>
            <div class="card-body">
                <h5 class="card-title">La teva comanda s'ha registrat correctament</h5>
                <a href="index.php" class="btn btn-confirm">Tornar a inici</a>
            </div>
        </div>
    </div>
</body>
</html>